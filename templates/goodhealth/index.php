<?php defined( '_JEXEC' ) or die( 'Restricted access' ); // no direct access ?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <jdoc:include type="head" />
  
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
  <link href="/templates/<?= $this->template ?>/css/jquery.formstyler.css" rel="stylesheet" />
  <link href="/templates/<?= $this->template ?>/css/styles.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <header id="js-header">
    <a href="/" class="logo">
      <img src="/templates/<?= $this->template ?>/img/logo.png" alt="GoodHealth" />
    </a>
    <section class="information">
      <p>От 1500 грн доставка бесплатная</p>
      <p>Онлайн заказы принимаем круглосуточно</p>
      <p>Скидки постоянным клиентам</p>
      <p>пн.-пт.: 9.00 - 19.00; сб.: 10.00 - 16.00</p>
    </section>
    <div class="vmCartModule">
        <jdoc:include type="modules" name="minicart" style="raw" />
    </div>
    <a class="cabinet-enter-button" href="#">Вход в личный кабинет</a>
    <section class="contacts">
      <p>(044) 206 206 9</p>
      <p>(044) 206 206 9</p>
      <p>пн.-пт.: 9.00 - 19.00; <span>сб.: 10.00 - 16.00</span></p>
    </section>
    <form class="search">
      <input type="text" placeholder="Поиск товара" />
      <input type="submit" value="Найти" />
    </form>
    <section class="mobile-bar" id="js-mobile-bar">
      <a href="#"></a>
      <form>
        <input type="text" placeholder="Поиск товара..." />
      </form>
    </section>
    <nav id="js-nav">
      <a href="#">Главная</a>
      <a href="#">О нас</a>
      <a href="#">Как сделать заказ</a>
      <a href="#">Доставка и оплата</a>
      <a href="#">Контакты</a>
      <a href="#">Фирмы-производители</a>
      <a href="#">Полезная информация</a>
      <a href="#">Прайс-лист</a>
      <a href="#">Быстрый заказ</a>
      <a href="#">Скидки</a>
    </nav>
  </header>
  <aside class="sidebar">
    <nav class="classification">
      <h2>Классификация</h2>
      <a href="#" class="by-sickness">По заболеваниям</a>
      <a href="#" class="by-organs">По органам и системам</a>
      <a href="#" class="by-action">По действию</a>
      <a href="#" class="by-components">По веществам</a>
    </nav>
    <nav class="production">
      <h2>Продукция</h2>
      <a href="#">American Health</a>
      <a href="#">Carlson Labs</a>
      <a href="#">ChildLife</a>
      <a href="#">Country Life</a>
      <a href="#">Enzymatic Therapy</a>
      <a href="#">Hairburst</a>
      <a href="#">Healthy Origins</a>
      <a href="#">Jarrow Formulas</a>
      <a href="#">Kirkman Labs</a>
      <a href="#">MHP</a>
      <a href="#">Natural Factors</a>
      <a href="#">Neocell</a>
      <a href="#">NOW Foods</a>
      <a href="#">Planetary Herbals</a>
      <a href="#">Primaforce</a>
      <a href="#">SeaBuckWonders</a>
      <a href="#">Sigma-tau</a>
      <a href="#">Solgar</a>
      <a href="#">Twinlab</a>
      <a href="#">Другие товары</a>
      <a href="#">Оздоровительные наборы</a>
      <a href="#">Наиболее востребованные продукты</a>
    </nav>
  </aside>
  <section class="content<?php if (!isset($_REQUEST['page'])) echo " frontpage"; ?>">
      <jdoc:include type="component" />
  </section>
  <footer>
    <section>
      <h3>Меню:</h3>
      <nav>
        <a href="#">О нас</a>
        <a href="#">Как сделать заказ</a>
        <a href="#">Доставка и оплата</a>
        <a href="#">Контакты</a>
        <a href="#">Фирмы-производители</a>
        <a href="#">Полезная информация</a>
        <a href="#">Прайс-лист</a>
        <a href="#">Быстрый заказ</a>
        <a href="#">Скидки</a>
      </nav>
    </section>
    <section>
      <h3>Контакты:</h3>
      <p>067-775-01-05<br />
      093-903-22-27</p>
      <p>Работаем:<br />
      Пн.-Пт.: 9.00-19.00<br />
      Сб.: 10.00-16.00<br />
      Вс.: выходной</p>
      <p>Адрес: Воздухофлотский пр. 16</p>
    </section>
    <section>
      <h3>Меню пользователя:</h3>
      <nav>
        <a href="#">Вход/Регистрация</a>
        <a href="#">Корзина</a>
      </nav>
    </section>
    <section>
      <h3>Поделиться в соц.сетях:</h3>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-vk.png" alt="..." /></a>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-fb.png" alt="..." /></a>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-twitter.png" alt="..." /></a>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-google-plus.png" alt="..." /></a>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-at.png" alt="..." /></a>
      <a href="#"><img src="/templates/<?= $this->template ?>/img/icon-mail.png" alt="..." /></a>
    </section>
    <section class="copyright">
      <p>Copyright 2010-2016 goodhealth.kiev.ua. Тексты сайта защищены законом об авторском праве.</p>
    </section>
  </footer>
  <section class="mobile-panel">
    <a href="#">Позвонить</a>
    <a href="#">Личный кабинет</a>
    <a href="#">Корзина</a>
  </section>
  <section class="modal-login">
      <header>
          Вход в личный кабинет
          <a href="#" class="close"></a>
      </header>
      <form>
          <div class="form-group">
              <label>E-mail:</label>
              <input type="input" />
          </div>
          <div class="form-group">
              <label>Пароль:</label>
              <input type="password" />
          </div>
          <div class="form-group">
              <input type="submit" value="Войти" />
              <div class="links">
                    <a class="remember-button" href="#">Напомнить пароль</a>
                    <a class="registration-button" href="#">Создать новый профиль</a>
              </div>
          </div>
      </form>
  </section>
  <section class="modal-registration">
      <header>
          Создание профиля
          <a href="#" class="close"></a>
      </header>
      <form>
          <div class="form-group">
              <label>Имя:</label>
              <input type="input" />
          </div>
          <div class="form-group">
              <label>E-mail:</label>
              <input type="input" />
          </div>
          <div class="form-group-container">
            <div class="form-group">
                <label>Пароль:</label>
                <input type="password" />
            </div>
            <div class="form-group">
                <label>Пароль ещё раз:</label>
                <input type="password" />
            </div>
          </div>
          <div class="form-group">
              <input type="submit" value="Создать новый профиль" />
          </div>
      </form>
  </section>
  <section class="modal-remember">
      <header>
          Напомнить пароль
          <a href="#" class="close"></a>
      </header>
      <form>
          <div class="form-group">
              <label>E-mail:</label>
              <input type="input" />
          </div>
          <div class="form-group">
              <p>Введите e-mail, который Вы указывали при регистрации.</p>
          </div>
          <div class="form-group">
              <input type="submit" value="Напомнить пароль" />
          </div>
      </form>
  </section>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="/templates/<?= $this->template ?>/js/scripts.js"></script>
</body>
</html>