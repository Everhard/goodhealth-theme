<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
mm_showMyFileName(__FILE__);

JLoader::import('joomla.application.component.model');
JLoader::import( 'catalog', JPATH_BASE . DS . 'components' . DS . 'com_ajax' . DS . 'models' );

$catalog = & JModel::getInstance('catalog', 'AjaxModel');
switch($catalog->getViewType()) {
    case AjaxModelCatalog::LIST_TYPE:
        $catalog_view_class = "list-view";
        $this->set("catalog_mode", "list-mode");
        break;
    case AjaxModelCatalog::GRID_TYPE:
        $catalog_view_class = "";
        $this->set("catalog_mode", "grid-mode");
        break;
}

?>

<?php echo $browsepage_header; // The heading, the category description ?>
<?php echo $orderby_form // The sort-by, order-by form PLUS top page navigation ?>

<section class="products-list <?php echo $catalog_view_class ?>">
    <?php foreach( $products as $product ) {
        foreach( $product as $attr => $val ) {
                // Using this we make all the variables available in the template
                // translated example: $this->set( 'product_name', $product_name );
                $this->set( $attr, $val );
        }
        
        echo $this->fetch( 'browse/browse.php' );
    }
    ?>
</section>

<?php echo $browsepage_footer; ?>