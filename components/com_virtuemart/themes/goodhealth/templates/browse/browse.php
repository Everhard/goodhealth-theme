<article>
    <h3><a href="<?php echo $product_flypage ?>"><?php echo $product_name; ?></a></h3>
    <p><?php echo $product_s_desc; ?></p>
    <figure>
        <?php echo ps_product::image_tag( $product_thumb_image, 'title="'.$product_name.'" alt="'.$product_name .'"' ) ?>
    </figure>
    <div class="price"><?php echo $product_price_raw['product_price']; ?> грн</div>
    <div class="rating"><img src="/templates/goodhealth/img/examples/rating.png" alt="" /></div>
    <button class="buy">В корзину</button>
</article>