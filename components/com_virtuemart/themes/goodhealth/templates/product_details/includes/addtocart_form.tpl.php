<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); ?>
    
<?php
mm_showMyFileName(__FILE__);
// This function lists all product children ( = Items)
// or, when not children are defined, the product_id
// SO LEAVE THIS IN HERE!
list($html,$children) = $ps_product_attribute->list_attribute( ( $product_parent_id > 0 )  ? $product_parent_id : $product_id );

if ($children != "multi") { 

    if( CHECK_STOCK == '1' && !$product_in_stock ) {
     	$notify = true;
    } else {
    	$notify = false;
    }

?>
    <form action="<?php echo $mm_action_url ?>index.php" method="post" name="addtocart" id="<?php echo uniqid('addtocart_') ?>" class="addtocart_form" <?php if( $this->get_cfg( 'useAjaxCartActions', 1 ) && !$notify ) { echo 'onsubmit="handleAddToCart( this.id );return false;"'; } ?>>

<?php
}
echo $html;

if (USE_AS_CATALOGUE != '1' && $product_price != "" && !stristr( $product_price, $VM_LANG->_('PHPSHOP_PRODUCT_CALL') )) {
	?>
        <?php if ($children != "multi") { ?> 

    <input type="number" id="js-quantity-counter" class="counter" value="1" min="1" />
    <button>В корзину</button>

    <?php  } ?>    
    <input type="hidden" name="flypage" value="shop.<?php echo $flypage ?>" />
    <input type="hidden" name="page" value="shop.cart" />
    <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id ?>" />
    <input type="hidden" name="category_id" value="<?php echo $category_id ?>" />
    <input type="hidden" name="func" value="cartAdd" />
    <input type="hidden" name="option" value="<?php echo $option ?>" />
    <input type="hidden" name="Itemid" value="<?php echo $Itemid ?>" />
    <input type="hidden" name="set_price[]" value="" />
    <input type="hidden" name="adjust_price[]" value="" />
    <input type="hidden" name="master_product[]" value="" />
    <input type="hidden" id="js-quantity-counter-field" name="quantity[]" value="" />
    <?php
}
if ($children != "multi") { ?>
	</form>
<?php } ?>