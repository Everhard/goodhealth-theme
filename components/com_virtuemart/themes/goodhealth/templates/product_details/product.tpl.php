<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
mm_showMyFileName(__FILE__);
 ?>
<h1><?php echo $product_name ?></h1>
<section class="breadcrumbs">
    <a href="/">Каталог товаров</a>
    <a href="<?php $sess->purl(URL."index.php?option=com_virtuemart&amp;page=shop.browse&amp;category_id=".$category_id) ?>">Now Foods</a>
    <a href="#"><?php echo $product_name ?></a>
</section>
<article class="product">
    <figure class="product-image">
        <?php
            $image_tag = ps_product::image_tag( $product_full_image, 'title="'.$product_name.'" alt="'.$product_name .'"', 1, 'product', 375, 375 );
            $image_tag = preg_replace("/((height|width)=\"\d+\")/", '', $image_tag);
            echo $image_tag;
        ?>
    </figure>
    <section class="product-card">
        <header><?php echo $product_name ?></header>
        <div class="rating">
            <img src="/templates/goodhealth/img/examples/icon-rating.png" alt="" />
            <span>(123 отзыва)</span>
        </div>
        <div class="buy-row">
            <span class="price"><?php echo $product_price_raw['product_price']; ?> грн</span>
            <?php echo $addtocart; ?>
        </div>
        <p><?php echo $product_s_desc; ?></p>
        <div class="buy-in-one-click">
            <input class="tel" type="text" placeholder="(050) 333-44-55" />
            <button>Купить в 1 клик</button>
        </div>
    </section>
    <section class="information">
        <h3 class="delivery">Доставка</h3>
        <ul>
            <li>самовывоз</li>
            <li>курьером по Киеву (40 грн)</li>
            <li>курьерской службой "Новая почта" в любой городу Украины (по тарифам перевозчика)</li>
        </ul>
        <h3 class="free-delivery">Бесплатная доставка</h3>
        <p>Если сумма Вашего заказа равна, или превышает 1500 грн.</p>
        <h3 class="payment">Оплата</h3>
        <ul>
            <li>наличными при получении товара</li>
            <li>на счет в ПриватБанке</li>
        </ul>
    </section>
</article>

<section class="product-details">
    <input id="description" type="radio" name="tabs" checked>
    <label class="description" for="description" title="Описание">Описание</label>

    <input id="reviews" type="radio" name="tabs">
    <label class="reviews" for="reviews" title="Отзывы">Отзывы</label>

    <section class="description-tab">
        <?php echo $product_description ?>
    </section>  
    <section class="reviews-tab">
        <form class="leave-review">
            <h3>Оставить отзыв</h3>
            <div class="form-group">
                <label>Имя:</label>
                <input type="text" />
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input type="email" />
            </div>
            <div class="form-group rating-mark">
                <span>Оцените товар:</span>
                <img src="/templates/goodhealth/img/examples/icon-rating.png" alt="" />
            </div>
            <div class="form-group">
                <label>Отзыв:</label>
                <textarea></textarea>
            </div>
            <input type="submit" value="Добавить отзыв" />
        </form>
        <section class="reviews-list">
            <article>
                <header>
                    <span class="name">Дарья Пупкина</span>
                    Дата отзыва: <span class="review-date">02.08.2016</span>
                </header>
                <div class="user-ratng-mark">Оценка: <img src="/templates/goodhealth/img/examples/icon-rating.png" alt="" /></div>
                <p>Фактически это действительно клей, который соединяет все клетки тела воедино. Это строительный блок для всех основных систем нашего организма и второе наиболее распространенное вещество в организме, уступая по объему только воде.</p>
            </article>
            <article>
                <header>
                    <span class="name">Дарья Пупкина</span>
                    Дата отзыва: <span class="review-date">02.08.2016</span>
                </header>
                <div class="user-ratng-mark">Оценка: <img src="/templates/goodhealth/img/examples/icon-rating.png" alt="" /></div>
                <p>Фактически это действительно клей, который соединяет все клетки тела воедино. Это строительный блок для всех основных систем нашего организма и второе наиболее распространенное вещество в организме, уступая по объему только воде.</p>
            </article>
            <article>
                <header>
                    <span class="name">Дарья Пупкина</span>
                    Дата отзыва: <span class="review-date">02.08.2016</span>
                </header>
                <div class="user-ratng-mark">Оценка: <img src="/templates/goodhealth/img/examples/icon-rating.png" alt="" /></div>
                <p>Фактически это действительно клей, который соединяет все клетки тела воедино. Это строительный блок для всех основных систем нашего организма и второе наиболее распространенное вещество в организме, уступая по объему только воде.</p>
            </article>
        </section>
    </section>
</section>
