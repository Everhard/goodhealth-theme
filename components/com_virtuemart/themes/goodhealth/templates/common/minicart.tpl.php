<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

if($empty_cart) { ?>
    <a class="cart-open-button" href="index.php?page=shop.cart&amp;option=com_virtuemart">Корзина <span>Ваша корзина пуста</span></a>
<?php } 
else {
    
    $products_count = 0;
    
    foreach( $minicart as $cart ) { 		

		foreach( $cart as $attr => $val ) {
			// Using this we make all the variables available in the template
			// translated example: $this->set( 'product_name', $product_name );
			$this->set( $attr, $val );
		}
        if(!$vmMinicart) { // Build Minicart
            
            $products_count += $cart['quantity'];
        }
    }
    
    ?>
    <a class="cart-open-button" href="index.php?page=shop.cart&amp;option=com_virtuemart">Корзина (<?=$products_count?>) <span>Сумма: <?=$total_price?></span></a>
<?php
} ?>