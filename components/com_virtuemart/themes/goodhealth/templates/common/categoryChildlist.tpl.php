<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
mm_showMyFileName(__FILE__);

if( empty( $categories )) {
	return; // Do nothing, if there are no child categories!
}
?>

<section class="brands">
    <?php foreach( $categories as $category ) { ?>
  <a href="<?php $sess->purl(URL."index.php?option=com_virtuemart&amp;page=shop.browse&amp;category_id=".$category["category_id"]) ?>" title="<?php echo $category["category_name"] ?>">
      <?= ps_product::image_tag( $category["category_thumb_image"], "alt=\"".$category["category_name"]."\"", 0, "category"); ?>
  </a>  
    <?php } ?>
</section>