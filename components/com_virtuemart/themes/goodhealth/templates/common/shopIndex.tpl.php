<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); ?>

<?php
defined( 'vmToolTipCalled') or define('vmToolTipCalled', 1); ?>
<h1>Интернет-магазин здоровья</h1>

<?php
$menu = & JSite::getMenu();
if ($menu->getActive() == $menu->getDefault()) { ?>
    <section class="slider">
      <img src="/templates/goodhealth/img/examples/slider.png" alt="" />
    </section>
    <section class="classification">
      <h2>Классификация препаратов</h2>
      <a href="#">По заболеваниям</a>
      <a href="#">По органам и системам</a>
      <a href="#">По действию</a>
      <a href="#">По веществам</a>
    </section>
<?php }

echo $vendor_store_desc;

echo $categories;

?>