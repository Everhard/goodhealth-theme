<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
 
class AjaxViewAjax extends JView {
    
    function display($tpl = null) {
        
        $model = & JModel::getInstance('catalog', 'AjaxModel'); 
        
        $response = array(
            "catalogView" => $model->getViewType(),
        );
        
        $this->assignRef("response", $response);
        
        parent::display($tpl);
    }
}