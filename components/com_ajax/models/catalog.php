<?php
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport( 'joomla.application.component.model' );
 
class AjaxModelCatalog extends JModel
{
    const LIST_TYPE = 1;
    const GRID_TYPE = 2;
    
    function __construct($config = array()) {
        parent::__construct($config);
        if (!isset($_SESSION['catalog-view'])) {
            $_SESSION['catalog-view'] = self::LIST_TYPE;
        }
    }
    
    function getViewType() {
        return $_SESSION['catalog-view'];
    }
    
    function setViewType($view_type) {
        if (in_array($view_type, array(self::LIST_TYPE, self::GRID_TYPE))) {
            $_SESSION['catalog-view'] = $view_type;
        }
    }
}