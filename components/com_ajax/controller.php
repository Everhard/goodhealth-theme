<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class AjaxController extends JController {
    function display() {
        parent::display();
    }
    
    function setCatalogView() {
        $model = & $this->getModel("catalog");
        $model->setViewType(JRequest::getVar('view_type'));
        parent::display();
    }
}