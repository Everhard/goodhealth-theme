# GoodHealth Theme #

### Как установить тему для PHP 5.2.17? ###
Не тестировалось.

### Как установить тему для PHP 5.3.29? ###

Первым делом отредактировать в configuration.php параметры: 
```
#!php

	var $log_path = '/var/www/goodhealth.kiev.ua/logs';
	var $tmp_path = '/var/www/goodhealth.kiev.ua/tmp';
```

Настроить в php.ini директивы, связанные с MySQL:
```
#!php
mysql.default_socket = /var/run/mysqld/mysqld.sock

```

Установка прав на запись для:
```
administrator/components/com_virtuemart/virtuemart.cfg.php
```

Установка темы "GoodHealth Theme 2016" в Расширения -> Менеджер шаблонов.

Установка темы "goodhealth" в Компоненты -> VirtueMart -> Настройки.Сайт -> Выберите шаблон для магазина: goodhealth.